//destructure the useState hook from React
import {useState, useEffect} from 'react';
import Course from "../components/Course";

// import coursesData from "../data/Courses";


export default function Courses(){

	const [coursesData, setCoursesData] = useState([])

	//Run one fetch request on Components mount(when the component loads for the first time)
	//Lifecycle of a component
	//1. Components mounts (initial load)
		//When a component mounts, all states are assigned their default values(this is considered a state change even if the component has no states)
	//2. Components rerenders (updates any necessary information about itself) again and again when needed

	//Always start writing useEffect like
	// useEffect(() => {

	//}, [])


	useEffect(()=>{
		fetch('http://localhost:4000/courses')
		.then(res=> res.json())
		.then(data=>{
			console.log(data)
			setCoursesData(data)
		})
	})
	/*
		useEffect is a React hook that allows any code inside of its scope to happen 
		when a component mounts (if array is empty) OR when a state changes (can have any number of states in array)

		By putting our fetch request inside of a useEffect, it will only fetch our data on 
		initial load/component mount (and not infinitely)
	*/

	const courses = coursesData.map(course =>{
			return(
				<Course key={course._id} courseProp={course}/>
			)
		})

	return(
		<>
			{courses}
		</>
	)
}