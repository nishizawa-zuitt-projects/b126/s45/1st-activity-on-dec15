import React from 'react'
import Banner from '../components/Banner'


export default function NotFound(){
	let bannerContent = {
		title: "Page Not found",
		description: "Page does not exist",
		buttonCallToAction: "Back to Home",
		destination:"/"
	}

	return(
		<Banner bannerProp={bannerContent}/>
	)
}



