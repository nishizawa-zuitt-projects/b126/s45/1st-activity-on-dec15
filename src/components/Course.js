/*
	In this file, first import Card and Button from react-bootstrap
	Create a Course component with the following features:
	One Card component
	Inside the card component, create a Card.Body component,
	Inside the Card.Body component, create a Card.Title component with 
	the name of a sample course(use any course name)
	Inside the Card.Body component, create a Card.Text component
	Inside the Card.Text component, create an h3 component with the word "description"
	Inside the Card.Text component, create a p component with ANY placeholder text
	below the h3 component
	Inside the card.text component, create an h4 component with the word "Price" below the component
	Inside the card.text component, create a p component with any price
	Inside the Card.text component, create a Button component with a variant attribute with a 
	value of "primary" that says "Enroll"
	Import the created Course Component in App.js. Comment out the Home component and add the 
	Course component in the return statement
*/
import {Card, Button} from 'react-bootstrap';
import {useState, useEffect} from 'react';

export default function Course({courseProp}){
	//props are also considered as states therefore, any change based on updated prop data,
	//Should have useEffect
	console.log(courseProp.name)
	//useState is an array that always has two elements. The first is the state,
	//which can be named anything. The second is the state setter, and can also be 
	// named anything, but Should be named similar to its partner state

	//e.g.
	//count/setCount, num/setNum, price/setPrice,
	//The two states always work together, with the state setter being the only way to change
	// the state
	//The value inside the parameter of useState() is the default state
	const [count, setCount] = useState(0)
	const [seats, setSeats]= useState(10)
	const [isOpen, setIsOpen] = useState(true)

	const {name, description, price} = courseProp

	useEffect(()=>{
		if(seats ===0){
			setIsOpen(false)
		}
	}, [seats])//Anything you add to the array inside of a useEffect is considered the state (or states) that it is managing

	const enrol = () =>{

		if(seats !== 0){
			setCount(count+1)
			setSeats(seats-1)
		}
	}

	return(
		<Card>
			<Card.Body>
				<Card.Title>
					<h2>{name}</h2>
				</Card.Title>
				<Card.Text>
					<h3>Description:</h3>
					<p>
					{description}
					</p>
					<h4>Price</h4>
					<p>{price}</p>
					<h4>Enrollees</h4>
					<p>{count}</p>
					<h4>Seats Left</h4>
					<p>{seats}</p>
				</Card.Text>
				{
					(isOpen)
					?
					<Button variant="primary" onClick={enrol}>Enroll</Button>
					:
					<Button variant="primary" disabled>Enroll</Button>
				}
			</Card.Body>
		</Card>
	)
}