import {Navbar, Nav} from 'react-bootstrap';

export default function AppNavbar(){
	return(
		<Navbar bg="light" expand="lg">
		  <Navbar.Brand href="#">Course Booking</Navbar.Brand>
		  <Navbar.Toggle aria-controls="basic-navbar-nav"/>
		  <Navbar.Collapse id="basic-navbar-nav">
		    <Nav className="ml-auto">
		      <Nav.Link href="#">Home</Nav.Link>
		      <Nav.Link href="#">Courses</Nav.Link>
		    </Nav>
		  </Navbar.Collapse>
		</Navbar>
	)
}