import {useState, useEffect} from 'react';
import {Form, Button, Container} from 'react-bootstrap';
import Swal from 'sweetalert2';

//Homework: Finish the register component based on shown screenshot output (all inside the form tags.)

// In App.js, import Register page component, comment out courses, and display Register

/*
Regarding events: user interaction such as loading page, clicking, typing, scrolling.
Click: Happens when a user clicks on an element
Submit: Happens when the form button is clicked or when the uer pressed the enter key once the form is all filled

Keypress: Happens when a user presses on a keyboard key
Onchange: Happens during Any changes in an input value
*/


export default function Register(){

	const [firstName, setFirstName] = useState("")
	const [lastName, setLastName] = useState("")
	const [email, setEmail] = useState("")
	const [mobileNo, setMobileNo] = useState("")
	const [password1, setPassword1] = useState("")
	const [password2, setPassword2] = useState("")
	const [isActive, setIsActive] = useState(false)

	useEffect(() =>{
		if((firstName !== "" && lastName !=="" && email !=="" && password1 !=="" && password2 !=="")&&(password1 === password2)){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [firstName, lastName, email, mobileNo, password1, password2])

	const registerUser = (e) =>{
		e.preventDefault()

		fetch('http://localhost:4000/users/checkEmail', {
			method: 'POST',
			headers:{
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})

		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
					title: "Duplicate email found",
					icon: "error",
					text: "Please provide a different email"
				})
			}else{
				fetch('http://localhost:4000/users/register', {
					method: 'POST',
					headers:{
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobileNo,
						email: email,
						password: password1
					})
				})
				.then(res => res.json())
				.then(result =>{
					if(result){
						Swal.fire({
							title: "Registration successfull",
							icon: "success",
							text: "You have been successfully registered"
						})
						setFirstName("")
						setLastName("")
						setEmail("")
						setMobileNo("")
						setPassword1("")
						setPassword2("")

					}else{
						Swal.fire({
							title: "Registration failed",
							icon: "error",
							text: "You have not been registered"						
						})
					}
				})
			}
		})
	}

	return(
		<Container>
		<Form className="m-3" onSubmit={e=> registerUser(e)}>
			<Form.Group controlId="firstName">
			  <Form.Label>First Name:</Form.Label>
			  <Form.Control type="text" placeholder="First Name" required value={firstName} onChange={e=> setFirstName(e.target.value)} />
			{/*e.taget.value = "The event's taget's value*/}
			</Form.Group>

			<Form.Group controlId="lastName">
			  <Form.Label>Last Name:</Form.Label>
			  <Form.Control type="text" placeholder="Last Name" required value={lastName} onChange={e=> setLastName(e.target.value)} />
			</Form.Group>

			<Form.Group controlId="email">
			  <Form.Label>Email address:</Form.Label>
			  <Form.Control type="email" placeholder="Enter email" required value={email} onChange={e=> setEmail(e.target.value)} />
			  <Form.Text className="text-muted">
			    We'll never share your email with anyone else.
			  </Form.Text>
			</Form.Group>

			<Form.Group controlId="mobileNo">
			  <Form.Label>Mobile Number:</Form.Label>
			  <Form.Control type="text" placeholder="Enter Mobile Number" required value={mobileNo} onChange={e=> setMobileNo(e.target.value)}/>
			</Form.Group>

			<Form.Group controlId="password1">
			  <Form.Label>Password:</Form.Label>
			  <Form.Control type="password" placeholder="Enter Password" required value={password1} onChange={e=> setPassword1(e.target.value)} />
			</Form.Group>

			<Form.Group controlId="password2">
			  <Form.Label>Verify Password:</Form.Label>
			  <Form.Control type="password" placeholder="Verify Password" required value={password2} onChange={e=> setPassword2(e.target.value)} />
			</Form.Group>

			{
				(isActive)
				?
				<Button variant="primary" type="submit">Submit</Button>
				:
				<Button variant="primary" disabled>Submit</Button>
			}
		</Form>
		</Container>
	)
}