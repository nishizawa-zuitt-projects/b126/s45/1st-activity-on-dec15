import {Jumbotron, Row, Col} from 'react-bootstrap'; //destucturing
import {Link} from 'react-router-dom'

export default function Banner(bannerProp){
	console.log(bannerProp)

	return(
		<Row>
			<Col>
				<Jumbotron className="text-center">
					<h1>{bannerProp.title}</h1>
					<p>{bannerProp.description}</p>
					<Link to={bannerProp.destination} className="btn btn-primary">{bannerProp.buttonCallToAction}</Link>
				</Jumbotron>
			</Col>
		</Row>
	)
}