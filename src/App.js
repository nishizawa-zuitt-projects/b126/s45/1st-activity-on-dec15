import AppNavbar from './components/AppNavbar';
import {BrowserRouter as Router} from "react-router-dom";
import {Routes,Route} from 'react-router-dom';
import Home from './pages/Home';
// import Course from './components/Course'
// import Courses from './pages/Courses'
// import Register from './pages/Register'
// import Login from './pages/Login'
import NotFound from './pages/NotFound'
import {Container} from 'react-bootstrap'
import './App.css';



//use destructuring to isolate only the react-bootstrap components that we need

//App entry point
//JSX
function App() {
  return (
  	<>	
  		<Router>
  			<AppNavbar />
  				<Container>
  				    <Routes>
  						<Route path="/" element={<Home />}/>
  						<Route element={<NotFound />}/>
  					</Routes>	
  				</Container>	        		
        </Router>
    </>
  );
}

export default App;
