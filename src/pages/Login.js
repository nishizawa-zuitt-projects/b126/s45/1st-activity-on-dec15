/* Optional takehome activity

Create a fully functioning login page that will display a sweetalert
window with a success message if the user logs in successfully (and also sets
all input values to an empty string), or a sweeetalert window with an
error message if the user does not.

Import this page to App.js and comment out the Register component to test.

*/

import {useState, useEffect} from 'react';
import {Form, Button, Container} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function Login(){
	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [isActive, setIsActive] = useState(false)

	useEffect(() =>{
		if(email !=="" && password !==""){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [email,password])

	const loginUser = (e) =>{
		e.preventDefault()

		fetch('http://localhost:4000/users/login', {
			method: "POST",
			headers:{
				'Content-Type':'application/json'
			},
			body: JSON.stringify({
				email:email,
				password:password
			})
		})
		.then(res => res.json())
		.then(data =>{

			if(data.access){
				localStorage.setItem('token', data.access)

				fetch('http://localhost:4000/users/details', {
					headers:{
						Authorization: `Bearer ${data.access}`
					}
				})
				.then(res => res.json())
				.then(data => {
					localStorage.setItem("id", data._id)
					localStorage.setItem("isAdmin", data.isAdmin)
					Swal.fire({
						title: "Login successfull",
						icon: "success",
						text: "You have been successfully registered"
					})
					setEmail("")
					setPassword("")
				})

			}else{
				Swal.fire({
					title: "Login failed",
					icon: "error",
					text: "You have not been registered or the password is incorrect"						
				})
			}
		})
	}


	return(
		<Container>
			<Form className="m-3" onSubmit={e=> loginUser(e)}>
			  <Form.Group controlId="email">
			    <Form.Label>Email address:</Form.Label>
			    <Form.Control type="email" placeholder="Enter email" required value={email} onChange={e=> setEmail(e.target.value)}/>
			  </Form.Group>

			  <Form.Group controlId="password">
			    <Form.Label>Password:</Form.Label>
			    <Form.Control type="password" placeholder="Password" required value={password} onChange={e=> setPassword(e.target.value)} />
			  </Form.Group>
			  {
			  	(isActive)
			  	?
			  	<Button variant="primary" type="submit">Login</Button>
			  	:
			  	<Button variant="primary" disabled>Login</Button>
			  }
			</Form> 
		</Container>
	)
}








